import PySimpleGUI as sg
import json
# layout = [[sg.Text('Enter a Number')],      
#           [sg.Input()],      
#           [sg.OK()] ]

# event, (number,) = sg.Window('Enter a number example', layout).Read()

# sg.Popup(event, number)

cat_define_dict = {}
output_file = 'cat_definition.json'
current_indx = 0
valid_cats = ['thoi-su', 'the-gioi', 'kinh-doanh', 'giai-tri', 'the-thao', 'phap-luat', 'giao-duc', 'suc-khoe', 'doi-song', 'du-lich', 'khoa-hoc', 'cong-nghe', 'xe']
picked_cat = None
layout = [
            [sg.Text('Filename')],      
            [sg.InputText(key='__IN_FILE__', size=(25, 2), font=('Helvetica', 10)), sg.FileBrowse()],
            [sg.OK()],
            [sg.Text('_'*25)], 
            [sg.Text('Category',size=(25,5)), sg.InputText(default_text='', key='origin-cat', size=(25,5), justification='center', font=('Arial', '16'))],
            [sg.Button('Thoi su', key='thoi-su'), sg.Button('The gioi',key='the-gioi'), sg.Button('Kinh doanh',key='kinh-doanh'), sg.Button('Giai tri',key='giai-tri'), 
                sg.Button('The thao',key='the-thao'), sg.Button('Phap luat',key='phap-luat'), sg.Button('Giao duc', key='giao-duc')], 
            [sg.Button('Suc khoe', key='suc-khoe'), sg.Button('Doi song', key='doi-song'), sg.Button('Du lich', key='du-lich'), 
                sg.Button('Khoa hoc', key='khoa-hoc'), sg.Button('Cong nghe', key='cong-nghe'), sg.Button('Xe', key='xe')],    
            [sg.Text('_'*25)], 
            [sg.Button('Save', key='save'), sg.Text('Processing Inx'), sg.Text('', key='current_indx')]
        ]


# event, (number,) = sg.Window('Get filename example', layout).Read()
window = sg.Window('Category Labeling', layout, return_keyboard_events=True, auto_size_text=True) 

# sg.Popup(event, number)

while (True):  
    # This is the code that reads and updates your window  
    event, values = window.Read()  
    if event in valid_cats:  
        # cat_define_dict[] = event
        # window.Element['thoi-su'].Update(button_color=('black', 'orange'))
        if(picked_cat in valid_cats):
            window.Element(picked_cat).Update(button_color=('white', sg.BLUES[0]))
        window.Element(event).Update(button_color=('black', 'orange'))
        picked_cat = event
        origin_cats_dict[origin_cat] = event
    elif event == 'q' or values is None:
        origin_cats_dict['LastProcessed'] = current_indx
        with open(file_path, 'w') as outfile:
            json.dump(origin_cats_dict, outfile)  
        break

    elif event == 'd':
        window.Element(picked_cat).Update(button_color=('white', sg.BLUES[0]))
        current_indx += 1
        window.Element('current_indx').Update(str(current_indx))
        origin_cat = origin_cats[current_indx]
        window.Element('origin-cat').Update(origin_cat)
        last_picked_cat = origin_cats_dict[origin_cat]
        if(last_picked_cat in valid_cats):
            picked_cat = last_picked_cat
            window.Element(picked_cat).Update(button_color=('black', 'orange'))

    elif event == 'a':
        window.Element(picked_cat).Update(button_color=('white', sg.BLUES[0]))
        current_indx -= 1
        window.Element('current_indx').Update(str(current_indx))
        origin_cat = origin_cats[current_indx]
        window.Element('origin-cat').Update(origin_cat)
        last_picked_cat = origin_cats_dict[origin_cat]
        if(last_picked_cat in valid_cats):
            picked_cat = last_picked_cat
            window.Element(picked_cat).Update(button_color=('black', 'orange'))

    elif(event == 'OK'):
        file_path = window.Element('__IN_FILE__').Get()
        with open(file_path) as f:
            origin_cats_dict = json.load(f)
        window.Element('__IN_FILE__').Update(disabled=True)
        origin_cats = list(origin_cats_dict.keys())
        try:
            current_indx = origin_cats_dict['LastProcessed']
        except:
            pass
        window.Element('current_indx').Update(str(current_indx))
        origin_cat = origin_cats[current_indx]
        window.Element('origin-cat').Update(origin_cat)
        last_picked_cat = origin_cats_dict[origin_cat]
        if(last_picked_cat in valid_cats):
            picked_cat = last_picked_cat
            window.Element(picked_cat).Update(button_color=('black', 'orange'))
        # print(event)

    elif event == 'save':
        origin_cats_dict['LastProcessed'] = current_indx
        # window.Element('current_indx').Update(str(current_indx))
        with open(file_path, 'w') as outfile:
            json.dump(origin_cats_dict, outfile)
        print('Save success')

window.Close()
